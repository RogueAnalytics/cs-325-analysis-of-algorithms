import random
import timeit

from homework_2.stoogesort import StoogeSort

if __name__ == '__main__':
    sortAlgo = {'Stooge_Sort': StoogeSort(None)}

    for name, algo in sortAlgo.items():
        filename = name + "_runtime.csv"
        with open(filename, "w") as runtime_file:
            runtime_file.write("number_of_items,runtime\n")
            timeArray = ()
            for num in [100, 500, 1000, 1500, 2000]:
                    sort_array = []
                    print(num)
                    for x in range(0, num):
                        sort_array.append(random.randint(0, 100000))
                    start_time = timeit.default_timer()
                    algo.sort(high_index=len(sort_array)-1, data=sort_array)
                    elapsed = timeit.default_timer() - start_time

                    runtime_file.write(str(num) + "," + str(elapsed) + '\n')
