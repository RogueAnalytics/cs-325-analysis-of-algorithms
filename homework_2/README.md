##Homework 2

Stooge Sort is organized into Python Packages. This allows me to use the package to assess the experimental
runtime without have several copies of both algorithms. Instructions to run each script follows.

- Running Stooge Sort on a file:

    - Command:
    
    ```bash
    $ python stoogesort [*Location of Files* (./homework_2/data.txt)]
    ```
    
	- Output:
	    StoogeSort Output.txt


- Experimental Runtime:

    - Command:
    
    ```bash
        python homework_2
    ```

    - Output:

        1. Stooge_Sort_runtime.csv
