import sys
from stoogesort.stooge_sort import StoogeSort

if __name__ == "__main__":
    print("Location of Data File?", sys.argv[1])
    with open(sys.argv[1], "r") as data_file:
        with open("StoogeSort Output.txt", "w") as merge_file:
            for line in data_file:
                current_line = list(map(int, line.split()))

                length = current_line[0]
                numbers = current_line[1:]

                stoogeSort = StoogeSort(numbers)

                sorted_array = stoogeSort.sort(high_index=len(numbers)-1)

                for integer in sorted_array:
                    merge_file.write(str(integer) + ' ')
                merge_file.write('\n')
