import math


class StoogeSort(object):
    name = 'Stooge Sort'

    def __init__(self, data):
        self.data = data

    """
    A Stooge Sort class for implementing the Stooge Sort Algorithm
    """
    def sort(self, high_index, low_index=0, data=None):
        """
        Sorts an array by StoogeSort implementation. Recurses sections of the array and return sorted values.

        :param data: A List of Integers
        :param high_index:
        :param low_index:
        :return: An array of integers from smallest to largest
        """
        if data is not None:
            self.data = data

        array_step_length = high_index - low_index + 1

        if array_step_length == 2 and self.data[low_index] > self.data[high_index]:
            temp = self.data[low_index]
            self.data[low_index] = self.data[high_index]
            self.data[high_index] = temp
        elif array_step_length > 2:
            group_length = int(math.ceil(2 * float(array_step_length) / 3))
            first_index = group_length - 1 + low_index
            second_index = array_step_length - group_length + low_index
            self.sort(high_index=first_index, low_index=low_index)
            self.sort(high_index=high_index, low_index=second_index)
            self.sort(high_index=first_index, low_index=low_index)
        return self.data


