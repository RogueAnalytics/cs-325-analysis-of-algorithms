 ##Homework 8

Run test for all Greedy Algorithms for NP-complete Knapsack

**The process file option relies on the answers not being located at
the bottom of the text file.** 

-
 Algorithm on a file:

    - Command:
    
    ```bash
    $ python3 -m homework_8.binpack [*Location of Files* (i.e., bin.txt)]
    ```
    
	- Output:
	    console text