from homework_8.binpack.bin_pack import BinFactory
from homework_8.binpack.first_fit import BinList
from homework_8.binpack.first_fit_dec import OrderedBinList
from homework_8.binpack.best_fit import BinHash
import timeit


class Test(object):
    def __init__(self, test_index, bin_size, items):
        self.test_id = test_index
        self.bin_size = bin_size
        self.bin_factory = BinFactory(bin_size)
        self.items = items
        self.methods = {'First Fit': {'method': BinList(self.bin_factory),
                                      'time': None
                                      },
                        'First Fit Decreasing': {'method': OrderedBinList(self.bin_factory),
                                                 'time': None
                                                 },

                        'Best Fit': {'method': BinHash(self.bin_factory),
                                     'time': None
                                     }
                        }

    def run(self):
        print('Test Case {} '.format(self.test_id), end=' ')
        for name, method in self.methods.items():
            start_time = timeit.default_timer()
            method['method'].process_items(items=self.items)
            method['time'] = timeit.default_timer() - start_time
        self.print()

    def print(self):
        for method in self.methods.values():
            print('{}: {}, {}'.format(method['method'].name, method['method'].bin_count, method['time']), end=', ')
        print('')
