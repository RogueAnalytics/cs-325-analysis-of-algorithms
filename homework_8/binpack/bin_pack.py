class Item(object):
    def __init__(self, line_item, size):
        self.id = line_item
        self.size = size


class Bin(object):
    def __init__(self, size):
        self.size = size
        self.items = []

    def add_item(self, item):
        self.items.append(item)

    @property
    def space_available(self):
        space_occupied = 0
        for item in self.items:
            space_occupied += item.size
        return self.size - space_occupied

    def print_bin(self):
        for item in self.items:
            print('{}: {}'.format(item.id, item.size), end='| ')
        print(' ')


class BinFactory(object):
    def __init__(self, size):
        self.size = size

    @property
    def generate(self):
        return Bin(self.size)
