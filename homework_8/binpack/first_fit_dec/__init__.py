from homework_8.binpack.first_fit import BinList


class MergeSort(object):
    name = 'Merge Sort'
    """
    An MergeSort class for implementing the MergeSort Algorithm
    """
    @staticmethod
    def _merge(left_list, right_list):
        """
        Takes two arrays and that number of integers that need to be returned

        :param left_list: An Array of Integers
        :param right_list: An Array of Integers
        :param data_length: Number of Integers to return
        :return: An Array of Integers
        """
        rv = []

        while len(left_list) != 0 and len(right_list) != 0:
            if left_list[0].size > right_list[0].size:
                rv.append(left_list.pop(0))
            else:
                rv.append(right_list.pop(0))

        if len(left_list) == 0:
            rv += right_list
        else:
            rv += left_list

        return rv

    def sort(self, items):
        """
        Sorts an array by splitting and sorting smaller arrays.

        :param items: A List of Integers
        :return: An array of integers from smallest to largest
        """
        if len(items) <= 1:
            return items
        else:
            split_point = int(len(items) / 2)

            return self._merge(
                     left_list=self.sort(items[split_point:]),
                     right_list=self.sort(items[:split_point])
                     )


class OrderedBinList(BinList):
    name = 'First Fit Decreasing'

    def __init__(self, bin_factory):
        super().__init__(bin_factory)

    def process_items(self, items):
        merge_sort = MergeSort()
        items = merge_sort.sort(items)
        return super().process_items(items)

    def print_bins(self):
        print('First Fit Decreasing: ', end='')
        for i in self.bins:
            i.print_bin()
