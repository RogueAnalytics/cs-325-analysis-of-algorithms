class BinList(object):
    name = 'First Fit'

    def __init__(self, bin_factory):
        self.bins = []
        self.bin_factory = bin_factory

    def add_item(self, item):
        selected_bin = self.get_bin(item.size)
        selected_bin.add_item(item)

    def get_bin(self, item_size):
        for i in self.bins:
            if item_size <= i.space_available:
                return i

        new_bin = self.bin_factory.generate
        self.bins.append(new_bin)
        return new_bin

    def process_items(self, items):
        for item in items:
            self.add_item(item)

    @property
    def bin_count(self):
        return len(self.bins)

    def print_bins(self):
        print('First Fit: ', end=' ')
        for bin in self.bins:
            bin.print_bin()