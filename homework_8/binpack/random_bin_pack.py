from homework_8.binpack.bin_pack import BinFactory
from homework_8.binpack.first_fit import BinList
from homework_8.binpack.first_fit_dec import OrderedBinList
from homework_8.binpack.best_fit import BinHash
from homework_8.binpack.bin_pack import Item
from homework_8.binpack.test_bin_pack import Test
import random
import csv

if __name__ == '__main__':
    tests = []
    for i in range(0, 1000):
        items = []
        bin_size = random.randint(1, 100)
        item_count = random.randint(1, 1000)
        for item in range(0, item_count):
            items.append(Item(item, random.randint(1, 100)))
        test = Test(i, bin_size, items)
        test.run()
        tests.append(test)
    with open('algo_analysis.csv', "w") as data_file:
        data_writer = csv.writer(data_file, delimiter=',', quotechar='"')
        test_index = 0
        for test in tests:
            test_index += 1
            for method in test.methods.values():
                data_writer.writerow([test_index, test.bin_size, len(test.items), method['method'].name, method['method'].bin_count, method['time']])



