class BinHash(object):
    name = 'Best Fit'

    def __init__(self, bin_factory):
        self.map = {}
        self.bin_factory = bin_factory

    def add_bin(self, bin):
        if bin.space_available in self.map:
            if self.map[bin.space_available]:
                self.map[bin.space_available].append(bin)
            else:
                self.map[bin.space_available] = [bin]
        else:
            self.map[bin.space_available] = [bin]

    def get_bin(self, space_available):
        return self.map[space_available].pop()

    def add_item(self, item):
        select_bin = self.get_best_fit(item.size)
        select_bin.add_item(item)
        self.add_bin(select_bin)

    def get_best_fit(self, item_size):
        fit_bin = [i for i in self.map.keys() if i >= item_size]
        if not fit_bin:
            return self.bin_factory.generate
        else:
            if self.map[min(fit_bin)]:
                return self.get_bin(min(fit_bin))
            else: return self.bin_factory.generate

    def process_items(self, items):
        for item in items:
            self.add_item(item)

    @property
    def bin_count(self):
        count = 0
        for value in self.map.values():
            count += len(value)
        return count

    def print_bins(self):
        print('BEST FIT')
        for bins in self.map.values():
            if bins:
                for bin in bins:
                    bin.print_bin()
        return
