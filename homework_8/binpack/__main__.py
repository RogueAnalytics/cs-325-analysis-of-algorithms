from homework_8.binpack.test_bin_pack import Test
from homework_8.binpack.bin_pack import Item

import sys

if __name__ == '__main__':
    print("Location of Data File?", sys.argv[1])
    with open(sys.argv[1], "r") as data_file:
        test_count = None
        current_test = 0
        bin_size = None
        item_count = None
        family_count = None
        item_line = False
        items = []
        tests = []

        for line in data_file:
            current_line = list(map(int, line.split()))
            if current_line:
                if test_count is None:
                    # print("Process Test Count: {}".format(current_line[0]))
                    test_count = current_line[0]
                elif bin_size is None:
                    # print("Process Bin Size: {}".format(current_line[0]))
                    bin_size = current_line[0]
                elif item_count is None:
                    # print("Process Item Count: {}".format(current_line[0]))
                    items = []
                    item_count = current_line[0]
                    item_index = 1
                    item_line = True
                elif item_line:
                    for index in range(0, item_count):
                        # print("Process Item {}".format(index))
                        items.append(Item(item_index, current_line[index]))
                    item_line = False
                    current_test += 1
                    tests.append(Test(current_test, bin_size, items))
                    if current_test <= test_count:
                        bin_size = None
                        item_count = None
                        items = []
    for test in tests:
        rv = test.run()

