loop through all objects (i = 1, 2...)
    loop through all bins (j = 1, 2, ...)
        if i.size fits in j.size_available
            put i to j
    if item didn't fit into a bin
        create new bin and pack object i

########################################
Approximate running time would be O(n^2) if every item had to go into a new bin.
