import sys

def product_list_value(list):
    """
    Helper function for retrieving total value of a list of products

    :param list:
    :return:
    """
    try:
        total_value = 0
        if list is None:
            return 0
        for item in list:
            total_value += item.value
    except:
        print(list)
    return total_value


class ProductWeightMatrix(object):
    """
    Generic Dynamic Programming Object
    """
    matrix = []

    def __init__(self, products, weight):
        """
        Create Matrix by building array from previously built weights. Exercise in Dynamic Programming
        :param products:
        :param weight:
        """
        x = -1
        self.matrix = []
        for i in range(0, len(products)):
            self.matrix.append([])
            for j in range(0, weight):
                self.matrix[i].append([])

        for product in products:
            x += 1
            for y in range(1, weight):
                if product.weight <= y:
                    remainingWeight = y - product.weight
                    products = self.matrix[x - 1][remainingWeight] + [product]

                    if product_list_value(products) > product_list_value(self.matrix[x - 1][y]):
                        self.matrix[x][y] = products
                    else:
                        self.matrix[x][y] = self.matrix[x - 1][y]
                else:
                    self.matrix[x][y] = self.matrix[x - 1][y]

    def get_product_list(self, weight):
        """
        Fetch the objects for a given weight in the matrix

        :param weight:
        :return:
        """
        return self.matrix[len(self.matrix) - 1][weight]


class Product(object):
    """
    Representation of Product
    """
    def __init__(self, index, value, weight):
        """
        Creation of a Product object from File at given index
        :param index: item index on file
        :param value: price of product
        :param weight: weight of product
        """
        self.value = value
        self.weight = weight
        self.index = index


class Member(object):
    """
    A Member of a Family
    """
    def __init__(self, index, weight):
        """
        Creation of Member of Family
        :param index:  item index in file
        :param weight: amount of weight individual can carry
        """
        self.weight = weight
        self.index = index


class Family(object):
    """
    Object for storing all family members
    """
    members = []

    def __init__(self):
        """
        Creation of Family
        """
        self.members = []

    def add_member(self, member):
        """
        Add Member to Family
        :param member:
        :return:
        """
        self.members.append(member)

    @property
    def family_size(self):
        """
        Number of individuals in family
        :return:
        """
        return len(self.members)

    def family_total_weight(self):
        """
        Amount of total weight for family. Potentially save time by calculating weight of entire family
        :return:
        """
        tw = 0
        for member in self.members:
            tw += member.weight
        return tw

    def family_max_weight(self):
        """
        Get largest weight carrying ability.
        :return:
        """
        mw = 0
        for member in self.members:
            if mw < member.weight:
                mw = member.weight
        return mw


class Test(object):
    def __init__(self, case_number, family, products):
        """
        Create Test for testing Shopping Algorithm

        :param case_number: index of test
        :param family: family for test
        :param products: all products for testing
        """
        self.case_number = case_number
        self.family = family
        self.products = products
        self.matrix = ProductWeightMatrix(products, family.family_max_weight() + 1)

    @property
    def total_price(self):
        """
        Calculate total price for all family members
        :return: total value of all products
        """
        tv = 0

        for member in self.family.members:
            tv += product_list_value(self.matrix.get_product_list(member.weight))
        return tv

    def run(self):
        """
        Run the test and output file
        :return:
        """
        with open("results.txt", "a+") as results:
            results.write("Test Case " + str(self.case_number) + '\n')
            results.write("Total Price  " + str(self.total_price) + '\n')
            for member in self.family.members:
                product_selection = self.matrix.get_product_list(member.weight)
                results.write(str(member.index) + ": ")
                for product in product_selection:
                    results.write(str(product.index) + ' ')
                results.write('\n')
            results.write('\n')
