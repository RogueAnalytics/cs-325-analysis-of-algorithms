import sys
import math


def product_list_value(list):
    """
    Helper function for retrieving total value of a list of products

    :param list:
    :return:
    """
    try:
        total_value = 0
        if list is None:
            return 0
        for item in list:
            total_value += item.value
    except:
        print(list)
    return total_value


class ProductWeightMatrix(object):
    """
    Generic Dynamic Programming Object
    """
    matrix = []

    def __init__(self, products, weight):
        """
        Create Matrix by building array from previously built weights. Exercise in Dynamic Programming
        :param products:
        :param weight:
        """
        x = -1
        self.matrix = []
        for i in range(0, len(products)):
            self.matrix.append([])
            for j in range(0, weight):
                self.matrix[i].append([])

        for product in products:
            x += 1
            for y in range(1, weight):
                if product.weight <= y:
                    remainingWeight = y - product.weight
                    products = self.matrix[x - 1][remainingWeight] + [product]

                    if product_list_value(products) > product_list_value(self.matrix[x - 1][y]):
                        self.matrix[x][y] = products
                    else:
                        self.matrix[x][y] = self.matrix[x - 1][y]
                else:
                    self.matrix[x][y] = self.matrix[x - 1][y]

    def get_product_list(self, weight):
        """
        Fetch the objects for a given weight in the matrix

        :param weight:
        :return:
        """
        return self.matrix[len(self.matrix) - 1][weight]


class Product(object):
    """
    Representation of Product
    """
    def __init__(self, index, value, weight):
        """
        Creation of a Product object from File at given index
        :param index: item index on file
        :param value: price of product
        :param weight: weight of product
        """
        self.value = value
        self.weight = weight
        self.index = index


class Member(object):
    """
    A Member of a Family
    """
    def __init__(self, index, weight):
        """
        Creation of Member of Family
        :param index:  item index in file
        :param weight: amount of weight individual can carry
        """
        self.weight = weight
        self.index = index


class Family(object):
    """
    Object for storing all family members
    """
    members = []

    def __init__(self):
        """
        Creation of Family
        """
        self.members = []

    def add_member(self, member):
        """
        Add Member to Family
        :param member:
        :return:
        """
        self.members.append(member)

    @property
    def family_size(self):
        """
        Number of individuals in family
        :return:
        """
        return len(self.members)

    def family_total_weight(self):
        """
        Amount of total weight for family. Potentially save time by calculating weight of entire family
        :return:
        """
        tw = 0
        for member in self.members:
            tw += member.weight
        return tw

    def family_max_weight(self):
        """
        Get largest weight carrying ability.
        :return:
        """
        mw = 0
        for member in self.members:
            if mw < member.weight:
                mw = member.weight
        return mw


class Test(object):
    def __init__(self, case_number, family, products):
        """
        Create Test for testing Shopping Algorithm

        :param case_number: index of test
        :param family: family for test
        :param products: all products for testing
        """
        self.case_number = case_number
        self.family = family
        self.products = products
        self.matrix = ProductWeightMatrix(products, family.family_max_weight() + 1)

    @property
    def total_price(self):
        """
        Calculate total price for all family members
        :return: total value of all products
        """
        tv = 0

        for member in self.family.members:
            tv += product_list_value(self.matrix.get_product_list(member.weight))
        return tv

    def run(self):
        """
        Run the test and output file
        :return:
        """
        with open("results.txt", "a+") as results:
            results.write("Test Case " + str(self.case_number) + '\n')
            results.write("Total Price  " + str(self.total_price) + '\n')
            for member in self.family.members:
                product_selection = self.matrix.get_product_list(member.weight)
                results.write(str(member.index) + ": ")
                for product in product_selection:
                    results.write(str(product.index) + ' ')
                results.write('\n')
            results.write('\n')



if __name__ == '__main__':
    print("Location of Data File?", sys.argv[1])
    with open(sys.argv[1], "r") as data_file:
        test_count = None
        current_test = 0

        product_count = None
        family_count = None

        product_line = False
        family_line = False
        products = []
        tests = []

        for line in data_file:
            current_line = list(map(int, line.split()))
            print(current_line)

            if test_count is None:
                print("Process Test")
                test_count = current_line[0]
            elif product_count is None:
                print("Process Product Count")
                products = []
                product_count = current_line[0]
                product_index = 1
                product_line = True
            elif product_line:
                print("Process Product")
                products.append(Product(product_index, current_line[0], current_line[1]))
                product_index += 1
                if len(products) < product_count:
                    product_line = True
                else:
                    product_line = False
            elif family_count is None:
                print("Process Family Count")
                family = Family()
                member_index = 1
                family_count = current_line[0]
                family_line = True
            elif family_line:
                print("Process Family Member")
                family.add_member(Member(member_index, current_line[0]))
                member_index += 1
                if family.family_size < family_count:
                    family_line = True
                else:
                    family_line = False

                    current_test += 1
                    tests.append(Test(current_test, family, products))
                    if current_test <= test_count:
                        family_count = None
                        product_count = None

    for test in tests:
        rv = test.run()
