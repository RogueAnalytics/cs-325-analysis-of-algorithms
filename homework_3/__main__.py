from shopping import *
import sys

if __name__ == '__main__':
    print("Location of Data File?", sys.argv[1])
    with open(sys.argv[1], "r") as data_file:
        test_count = None
        current_test = 0

        product_count = None
        family_count = None

        product_line = False
        family_line = False
        products = []
        tests = []

        for line in data_file:
            current_line = list(map(int, line.split()))
            print(current_line)

            if test_count is None:
                print("Process Test")
                test_count = current_line[0]
            elif product_count is None:
                print("Process Product Count")
                products = []
                product_count = current_line[0]
                product_index = 1
                product_line = True
            elif product_line:
                print("Process Product")
                products.append(Product(product_index, current_line[0], current_line[1]))
                product_index += 1
                if len(products) < product_count:
                    product_line = True
                else:
                    product_line = False
            elif family_count is None:
                print("Process Family Count")
                family = Family()
                member_index = 1
                family_count = current_line[0]
                family_line = True
            elif family_line:
                print("Process Family Member")
                family.add_member(Member(member_index, current_line[0]))
                member_index += 1
                if family.family_size < family_count:
                    family_line = True
                else:
                    family_line = False

                    current_test += 1
                    tests.append(Test(current_test, family, products))
                    if current_test <= test_count:
                        family_count = None
                        product_count = None

    for test in tests:
        rv = test.run()
