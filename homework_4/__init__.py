import sys


def activity_selection(activities, order):
    """
    Select first activity of sorted activities. Return a list of Compatible Activities and the selected activity.

    :param activities:
    :return: list of compatible activities, selected activity
    """
    selected_activity = activities.pop(0)
    for activity_index in range(0, len(activities)):
        if order == 'asc':
            if activities[0].start < selected_activity.finish:
                activities.pop(0)
        else:
            if activities[0].finish > selected_activity.start:
                activities.pop(0)

    return activities, selected_activity


class ActivityMergeSort(object):
    """
    An ActivityMergeSort class for implementing the ActivityMergeSort Algorithm on finish time

    Base on Homework 1
    """
    @staticmethod
    def _merge(left_list, right_list, field, order):
        """
        Takes two arrays and that number of integers that need to be returned

        :param left_list: An Array of Integers
        :param right_list: An Array of Integers
        :param data_length: Number of Integers to return
        :return: An Array of Integers
        """
        rv = []

        while len(left_list) != 0 and len(right_list) != 0:
            if order == 'asc':
                if getattr(left_list[0], field) < getattr(right_list[0], field):
                    rv.append(left_list.pop(0))
                else:
                    rv.append(right_list.pop(0))
            else:
                if getattr(left_list[0], field) > getattr(right_list[0], field):
                    rv.append(left_list.pop(0))
                else:
                    rv.append(right_list.pop(0))

        if len(left_list) == 0:
            rv += right_list
        else:
            rv += left_list

        return rv

    def sort(self, data, field, order='desc'):
        """
        Sorts an array by splitting and sorting smaller arrays.

        :param data: A List of Integers
        :param field: Field used for sorting Activities (start, finish)
        :return: An array of integers from smallest to largest
        """
        if len(data) <= 1:
            return data
        else:
            split_point = int(len(data) / 2)

            return self._merge(left_list=self.sort(data[split_point:], field),
                               right_list=self.sort(data[:split_point], field),
                               field=field,
                               order=order
                               )


class Activity(object):
    """
    An object representing an activity
    """
    activity_number = None
    start = None
    finish = None

    def __init__(self, activity_number, start, finish):
        """
        Load an activity with basic facts
        :param activity_number:
        :param start:
        :param finish:
        """
        self.activity_number = activity_number
        self.start = start
        self.finish = finish

    @property
    def length(self):
        return self.finish - self.start


class Test(object):
    """
    An object for storing the test information from the act.txt file
    """
    activities = []
    selected_activities = []
    sort_field = None
    order = 'desc'

    def __init__(self, test_number, activities, order):
        """
        Creation of basic activity file.

        :param test_number:
        :param activities:
        """
        self.test_number = test_number
        self.activities = activities
        self.order = order
        self.generate_schedule()

    def generate_schedule(self):
        """
        Generate the list of compatible events
        :return:
        """
        __selected_activities = []
        __activities = self.activities
        while len(__activities) > 0:
            __activities, selected = activity_selection(__activities, order=self.order)
            __selected_activities.insert(0, selected)
        self.selected_activities = __selected_activities


if __name__ == '__main__':
    print("Location of Data File?", sys.argv[1])
    merge_sort = ActivityMergeSort()
    activity_count = None
    activities = []
    tests = []
    test = 1

    with open(sys.argv[1], "r") as data_file:
        for line in data_file:
            current_line = list(map(str, line.split()))
            if activity_count is None:
                activity_count = int(current_line[0])
            else:
                activity_count -= 1
                activities.append(Activity(list(map(int, current_line[0].split('.')))[0], int(current_line[1]), int(current_line[2])))
                if activity_count == 0:
                    activities = merge_sort.sort(data=activities, field='start', order='desc')
                    tests.append(Test(test, activities, order='desc'))
                    activity_count = None
                    activities = []
                    test += 1

    for test in tests:
        print("Set {}".format(test.test_number))
        print("Number of Activities Selected = {}".format(len(test.selected_activities)))
        print("Activities: ", end='')
        for activity in test.selected_activities:
            print(activity.activity_number, end=' ')
        print("\n")


