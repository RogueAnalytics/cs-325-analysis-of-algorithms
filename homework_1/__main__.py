import random
import timeit

from insertsort import InsertSort
from mergesort import MergeSort


def fib_sequence():
    """
    Generate Fibonacci NUmbers
    - Source: Modeled after from https://www.programiz.com/python-programming/examples/fibonacci-sequence
    :return: list of fib numbers
    """
    last_number = 55
    current_number = 89
    times = [55, 89]

    for i in range(1, 15):
        next_number = last_number + current_number
        times.append(next)
        last_number = current_number
        current_number = next_number
    return times


if __name__ == '__main__':
    sortAlgo = {'merge': MergeSort(), 'insert': InsertSort()}

    for name, algo in sortAlgo.items():
        filename = name + "_runtime.csv"
        with open(filename, "w") as runtime_file:
            runtime_file.write("number_of_items,runtime\n")
            for num in fib_sequence():
                    sort_array = []
                    for x in range(0, num):
                        sort_array.append(random.randint(0, 10000000))
                    start_time = timeit.default_timer()
                    algo.sort(sort_array)
                    elapsed = timeit.default_timer() - start_time

                    runtime_file.write(str(num) + "," + str(elapsed) + '\n')