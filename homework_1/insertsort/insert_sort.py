class InsertSort(object):
    name = 'Insert Sort'
    """
    An InsertSort class for implementing the MergeSort Algorithm
    """
    @staticmethod
    def sort(data):
        """
        Implementation of the insert sort algorithm.
        # Identifies the location where the value should be placed.
        # Inserts value in location
        :param data:
        :return:
        """
        for sort_value_key in range(1, len(data)):
            sort_value = data.pop(sort_value_key)

            sorted_key = sort_value_key - 1
            while (sorted_key > -1) and (data[sorted_key] > sort_value):
                sorted_key -= 1

            data.insert(sorted_key+1, sort_value)
        return data
