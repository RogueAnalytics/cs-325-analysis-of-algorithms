class MergeSort(object):
    name = 'Merge Sort'
    """
    An MergeSort class for implementing the MergeSort Algorithm
    """
    @staticmethod
    def _merge(left_list, right_list):
        """
        Takes two arrays and that number of integers that need to be returned

        :param left_list: An Array of Integers
        :param right_list: An Array of Integers
        :param data_length: Number of Integers to return
        :return: An Array of Integers
        """
        rv = []

        while len(left_list) != 0 and len(right_list) != 0:
            if left_list[0] < right_list[0]:
                rv.append(left_list.pop(0))
            else:
                rv.append(right_list.pop(0))

        if len(left_list) == 0:
            rv += right_list
        else:
            rv += left_list

        return rv

    def sort(self, data):
        """
        Sorts an array by splitting and sorting smaller arrays.

        :param data: A List of Integers
        :return: An array of integers from smallest to largest
        """
        if len(data) <= 1:
            return data
        else:
            split_point = int(len(data) / 2)

            return self._merge(
                     left_list=self.sort(data[split_point:]),
                     right_list=self.sort(data[:split_point])
                     )