import sys
from merge_sort import MergeSort

if __name__ == "__main__":
    mergeSort = MergeSort()
    print("Location of Data File?", sys.argv[1])
    with open(sys.argv[1], "r") as data_file:
        with open("MergeSort Output.txt", "w") as merge_file:
            for line in data_file:
                current_line = list(map(int, line.split()))

                length = current_line[0]
                numbers = current_line[1:]

                sorted_array = mergeSort.sort(numbers)

                for integer in sorted_array:
                    merge_file.write(str(integer) + ' ')
                merge_file.write('\n')