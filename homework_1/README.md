##Homework 1

Merge Sort and Insert Sort are organized into Python Packages. This allows me to use the package to assess the experimental
runtime without have several copies of both algorithms. Instructions to run each script follows.

- Running Merge Sort on a file:

    - Command:
    
    ```bash
    $ python mergesort [*Location of Files* (./homework_1/data.txt)]
    ```
    
	- Output:
	    MergeSort Output.txt


- Running Insert Sort on a file

    - Command:

    ```bash
	    $ python insertsort [*Location of Files* (./homework_1/data.txt)]
    ```

	- Output:

	    InsertSort Output.txt

- Experimental Runtime:

    - Command:
    
    ```bash
        python homework_1
    ```

    - Output:

        1. merge_runtime.csv

        2. insert_runtime.csv
