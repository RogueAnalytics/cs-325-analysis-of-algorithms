# CS 325 - Analysis of Algorithms

##Homework 1

Merge Sort and Insert Sort are organized into Python Packages. This allows me to use the package to assess the experimental
runtime without have several copies of both algorithms. Instructions to run each script follows.

- Running Merge Sort on a file:

    - Command:
    
    ```bash
	    $ python3 homework_1/mergesort [*Location of Files* (./homework_1/data.txt)]
    ```
    
	- Output:
	
	    MergeSort Output.txt


- Running Insert Sort on a file

    - Command:
    
    ```bash
	    $ python3 homework_1/insertsort [*Location of Files* (./homework_1/data.txt)]
    ```
	- Output:

	    InsertSort Output.txt

- Experimental Runtime:

    - Command:

    ```bash
        $ python3 homework_1
    ```
    - Output:

        1. merge_runtime.csv

        2. insert_runtime.csv


##Homework 2

Merge Sort and Insert Sort are organized into Python Packages. This allows me to use the package to assess the experimental
runtime without have several copies of both algorithms. Instructions to run each script follows.

- Running Merge Sort on a file:

    - Command:
    
    ```bash
	    $ python3 homework_2/stoogesort [*Location of Files* (./homework_2/data.txt)]
    ```
    
	- Output:
	
	    StoogeSort Output.txt

- Experimental Runtime:

    - Command:

    ```bash
        $ python3 homework_2
    ```
    - Output:

        1. Stooge_Sort_runtime.csv
        
 ##Homework 3

Shopping Algorithm using Dynamic Programming

- Running shopping on a file:

    - Command:
    
    ```bash
    $ python3 homework_3 homework_3/shopping.txt [*Location of Files* (./homework_3/shopping.txt)]
    ```
    
	- Output:
	    results.txt

 ##Homework 4

Scheduling Activity Greedy Algorithm (last start time for activity). 

- Running activity selection on a file:

    - Command:
    
    ```bash
    $ python3 homework_4 homework_4/act.txt [*Location of Files* (./homework_4/act.txt)]
    ```
    
	- Output:
	    console text
	    
 ##Homework 5

DFS algorithm for assigning Wrestlers to a team. 

**The process file option relies on the answers not being located at
the bottom of the text file.** 

- Running selection on a file:

    - Command:
    
    ```bash
    $ python3 -m homework5.wrestling homework_5/wrestler.txt [*Location of Files* (./homework_5/wrestler1.txt)]
    ```
    
	- Output:
	    console text

 ##Homework 8

Run test for all Greedy Algorithms for NP-complete Knapsack

**The process file option relies on the answers not being located at
the bottom of the text file.** 

-
 Algorithm on a file:

    - Command:
    
    ```bash
    $ python3 -m homework_8.binpack [*Location of Files* (i.e., bin.txt)]
    ```
    
	- Output:
	    console text