import sys


class TeamDeterminationException(Exception):
    pass


class Team(object):
    def __init__(self, name):
        self.name = name
        self.wrestlers = []

    def add_wrestler(self, wrestler):
        self.wrestlers.append(wrestler)

    def output(self):
        print('{}'.format(self.name), end=': ')
        for wrestler in self.wrestlers:
            print(wrestler.name, end=' ')
        print('')


class Wrestler(object):
    def __init__(self, name):
        self.name = name
        self._team = None
        self.rivals = []

    def add_rival(self, rival):
        rival.rivals.append(self)
        self.rivals.append(rival)

    def set_team(self, team):
        if self._team is None:
            team.add_wrestler(self)
            self._team = team
        else:
            if self._team is not team:
                raise TeamDeterminationException

    @property
    def rival_count(self):
        return len(self._rivals.values)

    @property
    def team_name(self):
        return self._team.name


def assign_teams(teams, wrestlers, wrestler):
    try:
        for rival in wrestler.rivals:

            if wrestler.team_name == 'Babyfaces':
                rival.set_team(teams['Heels'])
            else:
                rival.set_team(teams['Babyfaces'])

            if rival.name in wrestlers:

                if rival.name in wrestlers:
                    del wrestlers[rival.name]

                wrestlers = assign_teams(teams, wrestlers, rival)
    except TeamDeterminationException:
        raise

    return wrestlers


def process_file(data_file, teams):
    first_wrestler = True
    wrestler_count = None
    rivalry_count = None
    wrestlers = {}

    for line in data_file:
        line = line.strip()
        current_line = list(map(str, line.rstrip().split(' ')))
        if wrestler_count is None:
            wrestler_count = int(current_line[0])
        elif wrestler_count > 0:
            wrestlers[current_line[0]] = Wrestler(current_line[0])
            if first_wrestler:
                wrestlers[current_line[0]].set_team(teams['Babyfaces'])
                first_wrestler = False
            wrestler_count -= 1
        elif rivalry_count is None:
            rivalry_count = int(current_line[0])
        elif rivalry_count > 0:
            wrestlers[current_line[0]].add_rival(wrestlers[current_line[1]])
            rivalry_count -= 1
        else:
            return wrestlers

def run(file):
    wrestlers = {}
    possible = True
    teams = {'Babyfaces': Team('Babyfaces'), 'Heels': Team('Heels')}
    print("Location of Data File?", file)
    with open(file, "r") as data_file:
        wrestlers = process_file(data_file, teams)

    try:
        while len(wrestlers) > 0:
            wrestler = wrestlers.pop(list(wrestlers.values())[0].name)
            wrestler.set_team(teams['Babyfaces'])
            wrestlers = assign_teams(teams, wrestlers, wrestler)

    except TeamDeterminationException:
        possible = False
    if possible:
        print('Yes Possible')
        for team in teams.values():
            team.output()
    else:
        print('No')


if __name__ == '__main__':
    run(sys.argv[1])