 ##Homework 5

DFS algorithm for assigning Wrestlers to a team. 

**The process file option relies on the answers not being located at
the bottom of the text file.** 

- Running Team Selection Algorithm on a file:

    - Command:
    
    ```bash
    $ python3 -m wrestling [*Location of Files* (i.e., wrestler1.txt)]
    ```
    
	- Output:
	    console text